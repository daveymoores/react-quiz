var path = require('path');
module.exports = {
   cache: true,
   debug: true,
   devtool: 'eval',
   entry: './src/app.js',
   module: {
   loaders: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader",
            query: {
                presets: ['react', 'es2015']
            }
        }
      ]
   },
   output: {
      path: path.join(__dirname, "build"),
      filename: 'build.min.js'
   },
   resolve: {
      extensions: ['', '.js', 'jsx', '.json']
   }
};
