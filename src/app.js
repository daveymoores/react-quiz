import React from 'react';
import ReactDOM from 'react-dom'; //only need to require react dom where the component is finally rendered

import {Router, Route, Redirect} from 'react-router';

import Layout from './js/layout/layout';

import Login from './js/pages/login';
import Quiz from './js/pages/quiz';
import Share from './js/pages/share';


const app = (
    <Router>
        <Redirect from="/" to="/login" />
        <Route path="/" component={Layout}>
            <Route path="/login" component={Login} />
            <Route path="/quiz" component={Quiz} />
            <Route path="/share" component={Share} />
        </Route>
    </Router>
)

ReactDOM.render(
    app,
    document.getElementById('app')
);
