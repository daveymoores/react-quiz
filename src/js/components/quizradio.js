import React from 'react';
import {RadioGroup, Radio} from 'react-radio-group';

export default class QuizRadio extends React.Component {
    constructor() {
        super();

        this.state = {
            selectedValue: 'apple' //get initial state now set in the constructor
        }
    }

    render() {
        return(
            <div className="quiz__type--radio">
                <RadioGroup name="fruit" selectedValue={this.state.selectedValue} onChange={this._updateValues.bind(this)}>
                    <Radio value="apple" />Apple
                    <Radio value="orange" />Orange
                    <Radio value="watermelon" />Watermelon
                </RadioGroup>
            </div>
        );
    }

    _updateValues(value) {
        this.setState({selectedValue: value});

        let data = {
            answer3 : value
        }

        this.props.saveValues(data);
    }
}
