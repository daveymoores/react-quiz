import React from 'react';

export default class QuizSelect extends React.Component {
    constructor (){
        super();
        this.state = {
            blue: 'blue',
            red: 'red',
            green: 'green'
        }
    }
    render() {
        return (
            <select ref="_answer4" onChange={this._updateValues.bind(this)}>
                <option value={this.state.blue}>{this.state.blue}</option>
                <option value={this.state.red}>{this.state.red}</option>
                <option value={this.state.green}>{this.state.green}</option>
            </select>
        );
    }

    _updateValues(e){
        let data = {
            answer4 : this.refs._answer4.value
        }

        this.props.saveValues(data);
    }
}
