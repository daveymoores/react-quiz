import React from 'react';

export default class QuizInput_2 extends React.Component { //must declare as default component
    constructor() {
        super();

        this.state = {
            characters: 0
        };
    }

    _setValue() {
        let data = {
            answer2 : this.refs._answer2.value
        }

        this.props.saveValues(data); //passes value to prop
        this.setState({
            characters: this.refs._answer2.value.length
        });
    }

    render() {
        return(
            <div className="quiz__type--input">
                <input type="text" ref="_answer2"
                defaultValue={this.props.fieldValues.answer_2}
                onChange={this._setValue.bind(this)} />
                <p>{this.state.characters} characters</p>
            </div>
        );
    }
}
