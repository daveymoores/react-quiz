import React from 'react';
import assign from 'object-assign'; //apparently react doesn't allow Object.assign

import QuizInput_1 from './quizinput_1';
import QuizInput_2 from './quizinput_2';
import QuizRadio from './quizradio';
import QuizSubmit from './quizsubmit';
import QuizSelect from './quizselect';



let fieldValues = {
    answer1 : null,
    answer2 : null,
    answer3 : null,
    answer4 : null,
    answer5 : null,
    answer6 : null,
    answer7 : null,
    answer8 : null,
    answer9 : null,
    answer10 : null
}


export default class QuizBox extends React.Component {
    constructor() {
        super();

        this.state = {
            buttonPrompt: "Click to complete quiz"
        };
    }

    saveValues(field_value){
        fieldValues = Object.assign({}, fieldValues, field_value);
        return console.log(fieldValues);
    }

    render () {
        return(
            <form action="" onSubmit={this._handleSubmit.bind(this)} className="row quiz__container">
                <h1>Quiz</h1>
                <div className="quiz__type--box">
                    <h3>Text input quiz type 1</h3>
                    <QuizInput_1 saveValues={this.saveValues} fieldValues={fieldValues} />
                </div>
                <div className="quiz__type--box">
                    <h3>Text input quiz type 2</h3>
                    <QuizInput_2 saveValues={this.saveValues} fieldValues={fieldValues} />
                </div>
                <div className="quiz__type--box">
                    <h3>Radio quiz type</h3>
                    <QuizRadio saveValues={this.saveValues} />
                </div>
                <div className="quiz__type--box">
                    <h3>Select quiz type</h3>
                    <QuizSelect saveValues={this.saveValues} />
                </div>
                <QuizSubmit text={this.state.buttonPrompt} />
            </form>
        );
    }

    _handleSubmit(e) {
        e.preventDefault();
        console.log(fieldValues);
        for(var value in fieldValues) {
            if(fieldValues[value] == null || fieldValues[value] == '') {
                return alert('Null value!');
            }
            return false;
        }
    }
}
