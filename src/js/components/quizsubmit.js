import React from 'react';

export default class QuizSubmit extends React.Component {
    constructor(){
        super();
    }

    render(){
        return (
            <button type="submit">{this.props.text}</button>
        );
    }
}
