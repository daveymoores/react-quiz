import React from 'react';
import { Link } from 'react-router';

export default class Layout extends React.Component {
    render(){
        return (
            <div>
                <div className="menu">
                    <ul className="menu__list">
                        <li>
                            <Link to="/login">Login</Link>
                        </li>
                        <li>
                            <Link to="/quiz">Quiz</Link>
                        </li>
                        <li>
                            <Link to="/share">Share</Link>
                        </li>
                    </ul>
                </div>

                {this.props.children}

            </div>
        )
    }
}
