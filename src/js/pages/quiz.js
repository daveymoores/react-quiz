import React from 'react';
import ReactDOM from 'react-dom';

import QuizBox from '../components/quizbox';

export default class Quiz extends React.Component {
    render (){
        return(
            <div>
                <h1>This is the quiz page</h1>
                <QuizBox />
            </div>
        )
    }
}
