# React Quiz App

A boilerplate for creating basic forms and quiz based apps with **React**.
The build process is using **gulp** and **webpack**, alongside **Babel**.

### usage ###

$ npm install

Then run...

$ gulp

Go to http://localhost:8080/webpack-dev-server/

### Folder structure ###

dist    
│
└───build
    │
    ├───css
    |
    |---build.min.js
